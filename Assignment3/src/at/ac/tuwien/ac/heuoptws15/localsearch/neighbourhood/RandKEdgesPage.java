package at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by felentovic on 06.11.16..
 */
public class RandKEdgesPage implements Neighbourhood {

    @Override
    public List<KPMPSolution> generate(KPMPSolution currentSolution) {
        //neki rand izmedu 2 i 0.1 m
        long max =  Math.round(0.3 * currentSolution.getMaxEgesOnPage());
        long min = 1;
        long randomNum = ThreadLocalRandom.current().nextLong(min, max);

        List<Pair> edges = new ArrayList<>();

        KPMPSolution clonedSolution = currentSolution.clone();
        //uzimat rand edgeve iz svake stranice
        for(Integer page : clonedSolution.getEdgePartitionOnPage().keySet()){
            int pageMax = clonedSolution.getEdgePartitionOnPage().get(page).size();
            long limit = Math.min(pageMax, randomNum);
            for (int i = 0; i < limit; i++){
                int randEdge = ThreadLocalRandom.current().nextInt(0,pageMax);
                Pair edge = getElementSet(clonedSolution.getEdgePartitionOnPage().get(page),randEdge);
                edges.add(edge);
                clonedSolution.removeEdge(edge);
                pageMax = clonedSolution.getEdgePartitionOnPage().get(page).size();
            }
        }

        List<KPMPSolution> neighbours = new LinkedList<>();
        while(neighbours.size() < 10) {
            KPMPSolution neighbour = clonedSolution.clone();

            for (int i = 0, size = edges.size(); i < size; i++) {
                int pageNum;
                if (i < size / 3) {
                    pageNum = ThreadLocalRandom.current().nextInt(neighbour.getK());

                } else {
                    pageNum = Utils.getPageWithMinCrossings(edges.get(i), neighbour);
                }
                Pair edge = edges.get(i);
                neighbour.addEdgeOnPage(edge.getStart(), edge.getEnd(), pageNum);
            }
            neighbours.add(neighbour);
        }
        return neighbours;
    }

    private Pair getElementSet(Set<Pair> edges, int index){
        int counter = 0;
        for (Pair edge: edges){
            if (counter == index){
                return edge;
            }
            counter++;
        }
        return null;
    }
}

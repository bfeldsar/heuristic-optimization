package at.ac.tuwien.ac.heuoptws15.localsearch;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.Neighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.StepFunction;

public class SimpleLocalSearch extends LocalSearch {
    private Integer maxIteration;

    public SimpleLocalSearch(Neighbourhood neighbourhood, StepFunction stepFunction, Integer maxIteration) {
        super(neighbourhood, stepFunction);
        this.maxIteration = maxIteration;
    }

    @Override
    void postIteration(KPMPSolution bestSolution, KPMPSolution previous) {
        // no work to do
    }

    @Override
    boolean stoppingCriteria(KPMPSolution bestSolution, KPMPSolution current, Integer iterations) {
        // if maxIteration limit is reached or neighbourhood did not produce better solution
//        System.out.println(iterations);
        return maxIteration > iterations && bestSolution != current;
    }
}

package at.ac.tuwien.ac.heuoptws15.variablesearch;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.StepFunction;

public class VariableNeighbourhoodDescent extends VariableNeighbourhoodSearch {
    public VariableNeighbourhoodDescent(StepFunction stepFunction) {
        super(stepFunction);
    }

    @Override
    public KPMPSolution search(KPMPSolution initialSolution) {
        int l = 0;
        int lmax = this.getNeighbourhoods().size();
        KPMPSolution bestSolution = initialSolution;
        while(l < lmax){
            KPMPSolution current = this.stepFunction.getSolution(this.getNeighbourhoods().get(l).generate(bestSolution), bestSolution);
            if (bestSolution.compareTo(current) > 0){
                bestSolution = current;
                l = 0;
            }else{
                l++;
            }
        }
        return bestSolution;
    }
}

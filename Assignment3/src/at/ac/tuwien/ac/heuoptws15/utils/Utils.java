package at.ac.tuwien.ac.heuoptws15.utils;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolution;

import java.util.*;
import java.util.stream.Collectors;

public class Utils {

    public static Double findDeviation(Collection<Long> collection) {
        Double mean = findMean(collection).getAsDouble();
        Double squareSum = 0.0;
        for (Long num : collection) {
            squareSum += Math.pow(num - mean, 2);
        }
        return Math.sqrt((squareSum) / (collection.size() - 1));
    }

    public static OptionalDouble findMean(Collection<Long> collection) {
        return collection.stream().mapToDouble(a -> a).average();
    }

    public static Long getCrossingsNumber(Map<Integer, Long> pageCrossings) {
        return pageCrossings.values().stream().mapToLong(Long::longValue).sum();
    }

    public static Set<Integer> getEmptyPages(Map<Integer, Set<Pair>> pageEdges) {
        return pageEdges.keySet().stream().filter(
                page -> pageEdges.get(page) == null || (pageEdges.get(page) != null && pageEdges.isEmpty())).collect(Collectors.toSet());
    }

    public static boolean existsCrossing(Pair edge1, Pair edge2, List<Integer> spineOrder) {
        if(edge1.equals(edge2)){
            return false;
        }
        Integer x11 = spineOrder.indexOf(edge1.start);
        Integer x12 = spineOrder.indexOf(edge1.end);
        Integer x21 = spineOrder.indexOf(edge2.start);
        Integer x22 = spineOrder.indexOf(edge2.end);
        return crossing(x11, x12, x21, x22) || crossing(x21, x22, x11, x12);
    }

    private static boolean crossing(Integer x11, Integer x12, Integer x21, Integer x22) {
        Integer min1 = Math.min(x11, x12);
        Integer max1 = Math.max(x11, x12);
        Integer min2 = Math.min(x21, x22);
        return min1 < min2 && min2 < max1 && Math.max(x21, x22) > max1;
    }

    public static List<Integer> getDFSSpineOrder(KPMPInstance instance) {
        List<Integer> spine = new ArrayList<>();
        Set<Integer> spineSet = new HashSet<>();
        Integer n = instance.getNumVertices();
        Integer current = n / 2;
        while (spine.size() != n) {
            spine.add(current);
            spineSet.add(current);
            List<Integer> neighbours = instance.getAdjacencyList().get(current);
            if ((current = getNotVisitedNeighbour(neighbours, spineSet)) == null) {
                current = getNotVisitedVertex(instance, spineSet);
            }
        }
        return spine;
    }

    private static Integer getNotVisitedVertex(KPMPInstance instance, Set<Integer> spineSet) {
        for (int i = 0; i<instance.getNumVertices(); i++){
            if (!spineSet.contains(i)){
                return i;
            }
        }
        return null;
    }

    private static Integer getNotVisitedNeighbour(List<Integer> neighbours, Set<Integer> spine) {
        for (Integer neighbour : neighbours) {
            if (!spine.contains(neighbour)) {
                return neighbour;
            }
        }
        return null;
    }

    public static Integer getPageWithMinCrossings(Pair pair, KPMPSolution solution) {
        Integer page = null;
        Long min = Long.MAX_VALUE;
        Set<Integer> emptyPages = Utils.getEmptyPages(solution.getEdgePartitionOnPage());
        Set<Integer> between = getVerticesBetween(pair, solution.getSpineIndexes(), solution.getSpineOrder());
        for (Map.Entry<Integer, Set<Pair>> entry : solution.getEdgePartitionOnPage().entrySet()) {
            Long crossings = getCrossingsOnPage(entry.getValue(), pair, between);
            if (crossings == 0){
                return entry.getKey();
            }
            if (crossings < min) {
                min = crossings;
                page = entry.getKey();
            }
        }
        if (!emptyPages.isEmpty()){
            page = emptyPages.iterator().next();
        }
        return page;
    }

    public static Integer getPageHeuristic(KPMPSolution solution){
        return solution.getEdgePartitionOnPage().entrySet().stream().min( (a, b) -> Integer.valueOf(a.getValue().size())
                .compareTo(b.getValue().size())).get().getKey();
    }


    public static Long getCrossingsOnPage(Set<Pair> edges, Pair pair, Set<Integer> between) {
        return edges.stream().filter(e -> crossing(between, pair, e)).count();
    }

    private static boolean crossing(Set<Integer> between, Pair pair, Pair e) {
        return !(pair.contains(e.getStart()) || pair.contains(e.getEnd())) &&
                ((between.contains(e.getStart()) && !between.contains(e.getEnd()))
                        || (between.contains(e.getEnd()) && !between.contains(e.getStart())));
    }


    public static long getAllCrossings(KPMPSolution neighbour) {
        long counter = 0;
        Set<Pair> visited1 = new HashSet<>();
        for (Set<Pair> edges2 : neighbour.getEdgePartitionOnPage().values()){
            for (Pair edge : edges2) {
                System.out.print(edge+" [");
                visited1.add(edge);
                for (Pair edge1 : edges2){
                    if (Utils.existsCrossing(edge, edge1, neighbour.getSpineOrder()) && !visited1.contains(edge1)){
                        counter++;
                        System.out.print(edge1+", ");
                    }
                }
                System.out.println("]");
            }
        }
        return counter;
    }

    public static List<Integer> getRandomSpineOrder(KPMPInstance instance) {
        List<Integer> spine = new ArrayList<>();
        Integer n = instance.getNumVertices();
        for (int i=0; i<n; i++){
            spine.add(i);
        }
        Collections.shuffle(spine);
        return spine;

    }

    public static Map<Integer, Set<Pair>> groupEdges(Map<Integer, Integer> spineMap, List<List<Integer>> adjacencyList) {
        Map<Integer, Set<Pair>> map = new HashMap<>();
        int n = spineMap.keySet().size();
        for (int i = 0; i <= n; i++){
            map.put(i, new HashSet<>());
        }
        Set<Integer> visited = new HashSet<>();
        for (int v1 = 0; v1 < n; v1++){
            visited.add(v1);
            for(Integer v2 : adjacencyList.get(v1)){
                if (!visited.contains(v2)){
                    map.get(Math.abs(spineMap.get(v1)-spineMap.get(v2))).add(new Pair(v1, v2));
                }
            }
        }
        return  map;
    }

    public static Set<Integer> getVerticesBetween(Pair oldPair, Map<Integer, Integer> spineIndexes, List<Integer> spineOrder) {
        Set<Integer> between = new HashSet<>();
        int start = spineIndexes.get(oldPair.getStart());
        int end = spineIndexes.get(oldPair.getEnd());
        int m = Math.max(start, end);
        for (int i=Math.min(start, end)+1; i < m; i++){
            between.add(spineOrder.get(i));
        }
        return between;
    }


}

package at.ac.tuwien.ac.heuoptws15.constructionheuristic;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import javax.rmi.CORBA.Util;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class DeterministicConstructionHeuristic implements ConstuctionHeuristic {

    private static List<Integer> getSortedSpineOrder(KPMPInstance instance) {
        List<VertexWithDegree> spine = new ArrayList<>();
        int i = 0;
        for (List<Integer> neighbours : instance.getAdjacencyList()) {
            spine.add(new VertexWithDegree(i++, neighbours.size()));
        }
        Collections.sort(spine);
        return spine.stream().map(vertexWithDegree -> vertexWithDegree.vertex).collect(Collectors.toList());
    }

    public KPMPSolution solve(KPMPInstance instance) throws IOException {
        KPMPSolution solution = new KPMPSolution(instance.getK(), instance.getAdjacencyMatrix());
        List<Integer> spineOrder = Utils.getDFSSpineOrder(instance);
        Set<Integer> visited = new HashSet<>();
        solution.setSpineOrder(spineOrder);
        solution.setAdjacencyList(instance.getAdjacencyList());
        for (Integer v1 : spineOrder) {
            visited.add(v1);
            for (Integer v2 : instance.getAdjacencyList().get(v1)) {
                if (!visited.contains(v2)) {
                    Integer page = Utils.getPageWithMinCrossings(new Pair(v2, v1), solution); // random.nextInt(instance.getK());
                    if (page == null) continue;
                    solution.addEdgeOnPage(v1, v2, page);
                }
            }
        }
        return solution;
    }

    public KPMPSolution solve2(KPMPInstance instance) throws IOException {
        KPMPSolution solution = new KPMPSolution(instance.getK(), instance.getAdjacencyMatrix());
        List<Integer> spineOrder = Utils.getDFSSpineOrder(instance);
        Map<Integer, Set<Pair>> pageEdges = new HashMap<>();
        for (int i = 0; i < instance.getK(); i++) {
            pageEdges.put(i, new HashSet<>());
            solution.getPageCrossings().put(i, 0L);
        }
        solution.setAdjacencyList(instance.getAdjacencyList());
        solution.setSpineOrder(spineOrder);
        List<Pair> pairsOnePage = new LinkedList<>();
        List<Pair> sortedPairs = getSortedEdgesByDistance(spineOrder, instance.getAdjacencyMatrix(), pairsOnePage);

        for(Pair pair : pairsOnePage){
            solution.addEdgeOnPage(pair.getStart(), pair.getEnd(), 0);
        }

        for (Pair pair : sortedPairs){
            Integer page = Utils.getPageWithMinCrossings(pair, solution);
            if (page == null) {
                System.out.println(page);
                continue;
            }
            solution.addEdgeOnPage(pair.getStart(), pair.getEnd(), page);
            Set<Pair> edges = pageEdges.get(page);
            edges.add(pair);
            pageEdges.put(page, edges);
        }

        return solution;
    }

    private List<Pair> getSortedEdgesByDistance(List<Integer> spineOrder, boolean[][] adjacencyMatrix, List<Pair> pairsOnePage) {
        Map<Integer, Integer> vertexIndex = new HashMap<>();
        for (int i = 0; i < spineOrder.size(); i++) {
            vertexIndex.put(spineOrder.get(i), i);
        }
        //po adjency matrix i dodavat parove (min, max) u mapu gdje je udaljenost key
        Map<Integer, Set<Pair>> distancePair = new HashMap<>();

        for (int i = 0, n = spineOrder.size() - 1; i < n; i++) {
            for (int j = i + 1; j < spineOrder.size(); j++) {
                    if(adjacencyMatrix[i][j]) {
                        int dist = Math.abs(vertexIndex.get(j) - vertexIndex.get(i));
                        Set<Pair> pairs = distancePair.getOrDefault(dist, new HashSet<>());
                        pairs.add(new Pair(i, j));
                        distancePair.put(dist, pairs);
                    }
            }
        }
        List<Integer> distances = new ArrayList<>();
        distances.addAll(distancePair.keySet());
        distances.sort((a, b) -> a.compareTo(b));

        List<Pair> sortedPairs = new LinkedList<>();

        int maxDist = spineOrder.size() - 1;
        for (Integer dist : distances){
            if(dist!= 1 && dist != maxDist) {
                sortedPairs.addAll(distancePair.get(dist));
            }
        }

        if (distancePair.containsKey(1)){
            pairsOnePage.addAll(distancePair.get(1));
        }
        if (distancePair.containsKey(maxDist)){
            pairsOnePage.addAll(distancePair.get(maxDist));
        }

        return sortedPairs;
    }

    private static class VertexWithDegree implements Comparable<VertexWithDegree> {
        Integer vertex;
        Integer degree;

        public VertexWithDegree(Integer vertex, Integer degree) {
            this.vertex = vertex;
            this.degree = degree;
        }

        @Override
        public int compareTo(VertexWithDegree o) {
            return degree.compareTo(o.degree);
        }

    }

}

package at.ac.tuwien.ac.heuoptws15.constructionheuristic;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolution;

import java.io.IOException;

public interface ConstuctionHeuristic {
    KPMPSolution solve(KPMPInstance instance) throws IOException;
}

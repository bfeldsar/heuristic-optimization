package at.ac.tuwien.ac.heuoptws15;

import at.ac.tuwien.ac.heuoptws15.constructionheuristic.ConstuctionHeuristic;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.DeterministicConstructionHeuristic;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.DeterministicGroupedEdgesConstructionHeuristic;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.DeterministicGroupedEdgesUpdatedConstructionHeuristic;
import at.ac.tuwien.ac.heuoptws15.localsearch.IteratedLocalSearch;
import at.ac.tuwien.ac.heuoptws15.localsearch.LocalSearch;
import at.ac.tuwien.ac.heuoptws15.localsearch.SimpleLocalSearch;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.*;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.BestImprovementStepFunction;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.FirstImprovementStepFunction;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;
import at.ac.tuwien.ac.heuoptws15.variablesearch.AdaptiveNeighbourhoodSearch;
import at.ac.tuwien.ac.heuoptws15.variablesearch.GeneralVariableNeighbourhoodSearch;
import at.ac.tuwien.ac.heuoptws15.variablesearch.VariableNeighbourhoodDescent;
import at.ac.tuwien.ac.heuoptws15.variablesearch.VariableNeighbourhoodSearch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        ConstuctionHeuristic strategy = new DeterministicGroupedEdgesUpdatedConstructionHeuristic();
        for (int j = 1; j < 11; j++) {
            if (j != 10) continue;
            String fileName = "automatic-"+j+".txt";
            KPMPSolution bestSolution = null;
            Long totalTime = 0L;
            List<Long> allCrossings = new ArrayList<>();
            final Integer N = 5;
            System.out.println(fileName);
            KPMPInstance instance = KPMPInstance.readInstance("./instances/"+fileName);
            KPMPSolution solution = strategy.solve(instance);
            System.out.println("Initial instance:  "+ solution.getCrossings());
            for (int k = 0; k < N; k++) {
//                LocalSearch localSearch = new IteratedLocalSearch(new EdgeOnLargestPageRearrangementNeighbourhood(),
                Long t1 = System.currentTimeMillis();
//                        new BestImprovementStepFunction(), 10, 100);
//                solution = localSearch.localSearch(solution);
//                AdaptiveNeighbourhoodSearch ads = new AdaptiveNeighbourhoodSearch();
                VariableNeighbourhoodSearch gvns = new GeneralVariableNeighbourhoodSearch();
                gvns.addNeighbourhood(new EdgeRearrangementNeighbourhood());
                gvns.addNeighbourhood(new RandKEdgesPage());
                gvns.addNeighbourhood(new EdgeKSwapNeighbourhood());
                gvns.addNeighbourhood(new VertexSwapNeighbourhood());
                solution = gvns.search(solution);
                if (bestSolution == null || bestSolution.compareTo(solution) > 0){
                    bestSolution = solution;
                }
                Long t2 = (System.currentTimeMillis() - t1);
                System.out.println((k+1)+". time  -  "+ t2+",  crossings - " +solution.getCrossings());
                allCrossings.add(solution.getCrossings());
                totalTime += t2;
            }
            System.out.println(fileName);
            System.out.println("Average time:  " + totalTime/N);
            System.out.println("Mean:  " + Utils.findMean(allCrossings).getAsDouble());
            System.out.println("Standard deviation:  " + Utils.findDeviation(allCrossings));
            System.out.println("best solution #crossings after local search: "+ bestSolution.getCrossings());
            System.out.println("------------------------------------------------------------------------------------------");
            KPMPSolutionWriter.write("./results/deterministic/"+fileName, bestSolution);
        }
    }
}

//System.out.println("Average time: "+ timeSum/N);
//System.out.println("Best result: " + Collections.min(crossingsPerRun));
//System.out.println("Mean: " + Utils.findMean(crossingsPerRun).getAsDouble());
//System.out.println("Standard deviation: " + Utils.findDeviation(crossingsPerRun));
//System.out.println("------------------------------------------------------------------------");
//bestSolution.write(Paths.get("./results/randomized/"+fileName).toString());

package at.ac.tuwien.ac.heuoptws15.genetic_alg;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.localsearch.LocalSearch;
import at.ac.tuwien.ac.heuoptws15.localsearch.SimpleLocalSearch;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.EdgeOnLargestPageRearrangementNeighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.VertexSwapNeighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.FirstImprovementStepFunction;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.*;
import at.ac.tuwien.ac.heuoptws15.variablesearch.VariableNeighbourhoodDescent;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class GeneticMain {

    private KPMPInstance instance;

    public GeneticMain(KPMPInstance instance) {
        this.instance = instance;
    }

    public KPMPSolution doSearch(KPMPInstance instance) throws IOException {
        int populationSize = 12;
        int maxiterations = 100;
        int numiterations = 0;
        int kTournament = 3;
        double mutationRate = 0.9;
        List<KPMPSolution> population = createPopulation(populationSize, instance);
        LocalSearch local_search = new SimpleLocalSearch(new VertexSwapNeighbourhood(), new FirstImprovementStepFunction(), 20);
        //fitness function
        do {
            List<KPMPSolution> childs = new ArrayList<>();
            while(childs.size() < populationSize){
                KPMPSolution parent1 = selection(population, kTournament);
                KPMPSolution parent2 = selection(population, kTournament);
                List<KPMPSolution> children = recombine(parent1, parent2);
                List<KPMPSolution> mutated = mutate(children, mutationRate);
                //mutated.stream().map((a)-> childs.add(local_search.localSearch(a))).collect();
                for(KPMPSolution tmp : mutated){
                    childs.add(local_search.localSearch(tmp));
                }
            }
            population.addAll(childs);
            //take best n, elitism
            population = population.stream().sorted((a,b) -> a.getCrossings().compareTo(b.getCrossings())).limit(populationSize).collect(Collectors.toList());
        } while (numiterations++ < maxiterations);
        Collections.sort(population);
        return population.get(0);
    }


    private double fitness(KPMPSolution solution) {
        return 1 / ((double) solution.getCrossings() + 1);
    }


    private KPMPSolution selection(List<KPMPSolution> population, int k) {
        List<KPMPSolution> populationCopy = new ArrayList<>(population);
        Collections.shuffle(populationCopy);
        populationCopy = populationCopy.stream().limit(k).collect(Collectors.toList());
        Collections.sort(populationCopy);
        return populationCopy.get(0);
    }

    //two-point crossover
    private List<KPMPSolution> recombine(KPMPSolution parent1, KPMPSolution parent2) {
        List<KPMPSolution> children = new ArrayList<>();

        List<Integer> child1Spine = crossoverSpine(parent1, parent2);
        KPMPSolution child1 = createChildWithNewSpine(parent1, child1Spine);

        crossoverEdges(child1, parent2);

        List<Integer> child2Spine = crossoverSpine(parent2, parent1);
        KPMPSolution child2 = createChildWithNewSpine(parent2, child2Spine);

        crossoverEdges(child2, parent1);

        children.add(child1);
        children.add(child2);
        return children;
    }

    private KPMPSolution createChildWithNewSpine(KPMPSolution parent, List<Integer> spineOrder) {
        KPMPSolution child = new KPMPSolution(parent.getK(), parent.getAdjacencyMatrix(), parent.getAdjacencyList());
        child.setSpineOrder(spineOrder);
        for (Map.Entry<Pair, Integer> entry : parent.getEdgePage().entrySet()) {
            Pair pair = entry.getKey();
            int page = entry.getValue();
            child.addEdgeOnPage(pair.getStart(), pair.getEnd(), page);
        }
        return child;
    }

    private List<Integer> crossoverSpine(KPMPSolution parent1, KPMPSolution parent2) {
        //first k and the last k elements are preserved from parent1
        int n1 = parent1.getSpineOrder().size();
        int k = n1 / 4;
        //random number in range 0-k
        k = ThreadLocalRandom.current().nextInt(1,k + 1);

        List<Integer> newSpine = new ArrayList<>();
        newSpine.addAll(parent1.getSpineOrder().stream().limit(k).collect(Collectors.toList()));

        List<Integer> lastK = new ArrayList<>();
        List<Integer> parent1Spine = parent1.getSpineOrder();
        for (int i = n1 - k; i < n1; i++) {
            lastK.add(parent1Spine.get(i));
        }

        List<Integer> parent2Spine = parent2.getSpineOrder();
        for (Integer v : parent2Spine){
            if (!lastK.contains(v) && !newSpine.contains(v)){
                newSpine.add(v);
            }
        }
        newSpine.addAll(lastK);
        return newSpine;
    }

    private KPMPSolution crossoverEdges(KPMPSolution child, KPMPSolution parent2) {
        int k = parent2.getEdgePage().size() / 4;
        Map<Pair, Integer> clonedMap = (Map<Pair, Integer>) child.getEdgePage().clone();
        for (Pair edge: getRandomN(new ArrayList<>(clonedMap.keySet()), k)) {
            child.removeEdge(edge);
            child.addEdgeOnPage(edge.getStart(), edge.getEnd(), parent2.getEdgePage().get(edge));
        }

        return child;
    }
    private List<KPMPSolution> mutate(List<KPMPSolution> population, Double mutationRate) {
        List<KPMPSolution> mutated = new ArrayList<>();
        Random random = new Random();
        for (KPMPSolution s : population){
            mutated.add(random.nextInt(100) < mutationRate*100 ? mutate(s) : s);
        }
        return mutated;
    }

    private KPMPSolution mutate(KPMPSolution solution) {
        int n = (int) (solution.getEdgePage().size()*0.1 + 1);
        Random random = new Random();
        Integer spineSize = solution.getSpineOrder().size();
        int a = random.nextInt(spineSize);
        int b;
        do{
            b = random.nextInt(spineSize);
        }while (b == a);
        swapVerteciesOnSpine(a, b, solution, random);
//        for(Pair p : getRandomN(new ArrayList<>(solution.getEdgePage().keySet()), n)){
//            solution.removeEdge(p);
//            solution.addEdgeOnPage(p.getStart(), p.getEnd(), random.nextInt(solution.getK()));
//        }
        return solution;
    }

    private List<Pair> getRandomN(List<Pair> pairs, int n) {
        Collections.shuffle(pairs);
        return pairs.stream().limit(5).collect(Collectors.toList());
    }


    private List<KPMPSolution> createPopulation(int size, KPMPInstance instance) throws IOException {
        List<KPMPSolution> population = new ArrayList<>();

        population.add(new DeterministicConstructionHeuristic().solve(instance));
        size--;
        population.add(new DeterministicGroupedEdgesConstructionHeuristic().solve(instance));
        size--;
        population.add(new DeterministicGroupedEdgesUpdatedConstructionHeuristic().solve(instance));
        size--;
        population.add(new DeterministicUpdatedConstructionHeuristic().solve(instance));
        size--;

        while (size-- > 0) {
            population.add(new RandomPagesConstructionHeuristic().solve(instance));
        }
        return population;
    }

    private void swapVerteciesOnSpine(int a, int b, KPMPSolution neighbour, Random random) {

        // before swap
        int vertexA = neighbour.getSpineOrder().get(a);
        int vertexB = neighbour.getSpineOrder().get(b);
        Set<Pair> edges = findIncidentEdges(vertexA, vertexB, neighbour);
        removeIncidentEdges(edges, neighbour);

        // swap two vertices
        List<Integer> spine = neighbour.getSpineOrder();
        Integer temp = vertexA;
        spine.add(a, vertexB);
        spine.remove(a+1);
        spine.add(b, temp);
        spine.remove(b+1);

        // after swap
        for (Pair p : edges) {
            neighbour.addEdgeOnPage(p.getStart(), p.getEnd(), random.nextInt(neighbour.getK()));
        }
    }

    private void removeIncidentEdges(Set<Pair> edges, KPMPSolution neighbour) {
        for (Pair edge : edges){
            neighbour.removeEdge(edge);
        }
    }

    private Set<Pair> findIncidentEdges(int vertexA, int vertexB, KPMPSolution neighbour) {
        Set<Pair> edges = new HashSet<>();
        for (Integer adjacentVertex : neighbour.getAdjacencyList().get(vertexA)){
            edges.add(new Pair(adjacentVertex, vertexA));
        }
        for (Integer adjacentVertex : neighbour.getAdjacencyList().get(vertexB)){
            edges.add(new Pair(adjacentVertex, vertexB));
        }
        return edges;
    }

}

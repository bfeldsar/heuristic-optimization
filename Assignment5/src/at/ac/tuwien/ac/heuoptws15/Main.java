package at.ac.tuwien.ac.heuoptws15;

import at.ac.tuwien.ac.heuoptws15.constructionheuristic.ConstuctionHeuristic;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.DeterministicGroupedEdgesUpdatedConstructionHeuristic;
import at.ac.tuwien.ac.heuoptws15.genetic_alg.GeneticMain;
import at.ac.tuwien.ac.heuoptws15.localsearch.LocalSearch;
import at.ac.tuwien.ac.heuoptws15.localsearch.SimpleLocalSearch;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.VertexSwapNeighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.FirstImprovementStepFunction;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        ConstuctionHeuristic strategy = new DeterministicGroupedEdgesUpdatedConstructionHeuristic();
        for (int j = 1; j < 11; j++) {
            if (j > 5) continue;
            String fileName = "automatic-"+j+".txt";
            KPMPSolution bestSolution = null;
           // KPMPSolution bestSolution_ls = null;
            Long totalTime = 0L;
            List<Long> allCrossings = new ArrayList<>();
            final Integer N = 5;
            System.out.println(fileName);
            KPMPInstance instance = KPMPInstance.readInstance("./instances/"+fileName);
//            KPMPSolution solution = strategy.solve(instance);
            for (int k = 0; k < N; k++) {
                Long t1 = System.currentTimeMillis();
                KPMPSolution solution = new GeneticMain(instance).doSearch(instance);
               // KPMPSolution solution_ls = new SimpleLocalSearch(new VertexSwapNeighbourhood(),new FirstImprovementStepFunction(),15).localSearch(solution);
                if (bestSolution == null || bestSolution.compareTo(solution) > 0){
                    bestSolution = solution;
                 //   bestSolution_ls = solution_ls;
                }
                Long t2 = (System.currentTimeMillis() - t1);
                System.out.println((k+1)+". time  -  "+ t2+",  crossings ea:" +solution.getCrossings()+", crossings ls:"+solution.getCrossings() );
                allCrossings.add(solution.getCrossings());
                totalTime += t2;
            }
            System.out.println(fileName);
            System.out.println("Average time:  " + totalTime/N);
            System.out.println("Mean:  " + Utils.findMean(allCrossings).getAsDouble());
            System.out.println("Standard deviation:  " + Utils.findDeviation(allCrossings));
            System.out.println("best solution #crossings after evolution alg: "+ bestSolution.getCrossings());
           // System.out.println("best solution #crossings after local search: "+ bestSolution_ls.getCrossings());
            System.out.println("------------------------------------------------------------------------------------------");
            KPMPSolutionWriter.write("./results/deterministic/"+fileName, bestSolution);
        }
    }
}

//System.out.println("Average time: "+ timeSum/N);
//System.out.println("Best result: " + Collections.min(crossingsPerRun));
//System.out.println("Mean: " + Utils.findMean(crossingsPerRun).getAsDouble());
//System.out.println("Standard deviation: " + Utils.findDeviation(crossingsPerRun));
//System.out.println("------------------------------------------------------------------------");
//bestSolution.write(Paths.get("./results/randomized/"+fileName).toString());

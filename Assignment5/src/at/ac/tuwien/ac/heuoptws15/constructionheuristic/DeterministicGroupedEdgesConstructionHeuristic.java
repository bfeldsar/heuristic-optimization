package at.ac.tuwien.ac.heuoptws15.constructionheuristic;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.io.IOException;
import java.util.*;

public class DeterministicGroupedEdgesConstructionHeuristic implements ConstuctionHeuristic {

    @Override
    public KPMPSolution solve(KPMPInstance instance) throws IOException {
        KPMPSolution solution = new KPMPSolution(instance.getK(), instance.getAdjacencyMatrix(), instance.getAdjacencyList());        List<Integer> spineOrder = Utils.getDFSSpineOrder(instance);
        solution.setSpineOrder(spineOrder);
        solution.setAdjacencyList(instance.getAdjacencyList());
        Map<Integer, Integer> spineMap = new HashMap<>();
        for (int i = 0; i < spineOrder.size(); i++){
            spineMap.put(spineOrder.get(i), i);
        }
        Map<Integer, Set<Pair>> groupedEdges = Utils.groupEdges(spineMap, instance.getAdjacencyList());
        for (int i = spineOrder.size(); i >= 0 ; i--){
            for(Pair pair : groupedEdges.get(i)){
                Integer page = Utils.getPageWithMinCrossings(pair, solution); // random.nextInt(instance.getK());
                if (page == null) continue;
                solution.addEdgeOnPage(pair.getStart(), pair.getEnd(), page);
            }
        }
        return solution;
    }
}

package at.ac.tuwien.ac.heuoptws15.constructionheuristic;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.io.IOException;
import java.util.*;

public class DeterministicUpdatedConstructionHeuristic implements ConstuctionHeuristic {

    @Override
    public KPMPSolution solve(KPMPInstance instance) throws IOException {
        KPMPSolution solution = new KPMPSolution(instance.getK(), instance.getAdjacencyMatrix(), instance.getAdjacencyList());        List<Integer> spineOrder = Utils.getDFSSpineOrder(instance);
        solution.setSpineOrder(spineOrder);
        solution.setAdjacencyList(instance.getAdjacencyList());
        Set<Integer> visited = new HashSet<>();
        for (Integer v1: spineOrder){
            visited.add(v1);
            for(Integer v2 : instance.getAdjacencyList().get(v1)){
                if (!visited.contains(v2)){
                    Integer page = Utils.getPageWithMinCrossings(new Pair(v2, v1), solution); // random.nextInt(instance.getK());
                    if (page == null) continue;
                    solution.addEdgeOnPage(v1, v2, page);
                }
            }
        }
        return solution;
    }
}

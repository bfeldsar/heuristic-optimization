package at.ac.tuwien.ac.heuoptws15.utils;

public class PageEntry {
    public int a, b;
	public int page;

	public PageEntry(int a, int b, int page) {
		this.a = a;
		this.b = b;
		this.page = page;
	}
}
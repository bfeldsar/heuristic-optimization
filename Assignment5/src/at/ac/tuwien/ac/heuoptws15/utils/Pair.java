package at.ac.tuwien.ac.heuoptws15.utils;

public class Pair {
    int start;
    int end;

    public Pair(int start, int end){
        this.start = Math.min(start, end);
        this.end = Math.max(start,end);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair pair = (Pair) o;

        return equals(pair) || equals(new Pair(pair.end, pair.start));
    }

    public int getEnd() {
        return end;
    }

    public int getStart() {
        return start;
    }

    private boolean equals(Pair pair) {
        return start == pair.start && end == pair.end;
    }

    public boolean contains(int vertex) {
        return start == vertex || end == vertex;
    }

    @Override
    public int hashCode() {
        int result = start;
        result = 31 * result + end;
        return result;
    }

    @Override
    public String toString() {
        return "("+this.start+","+this.end+")";
    }
}

package at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;

import java.util.List;

public interface Neighbourhood {
    List<KPMPSolution> generate(KPMPSolution currentSolution);
}

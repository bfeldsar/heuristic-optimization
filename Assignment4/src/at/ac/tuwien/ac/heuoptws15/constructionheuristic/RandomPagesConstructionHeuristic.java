package at.ac.tuwien.ac.heuoptws15.constructionheuristic;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.io.IOException;
import java.util.*;

public class RandomPagesConstructionHeuristic implements ConstuctionHeuristic {

    public KPMPSolution solve(KPMPInstance instance) throws IOException {
        KPMPSolution solution = new KPMPSolution(instance.getK(), instance.getAdjacencyMatrix(), instance.getAdjacencyList());        List<Integer> spineOrder = Utils.getRandomSpineOrder(instance);
        solution.setSpineOrder(spineOrder);
        solution.setAdjacencyList(instance.getAdjacencyList());
        Set<Integer> visited = new HashSet<>();
        Random random = new Random();
        for (Integer v1 : spineOrder) {
            visited.add(v1);
            for (Integer v2 : instance.getAdjacencyList().get(v1)) {
                if (!visited.contains(v2)) {
                    solution.addEdgeOnPage(v1, v2, random.nextInt(instance.getK()));
                }
            }
        }
        return solution;
    }
}

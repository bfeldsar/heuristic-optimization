package at.ac.tuwien.ac.heuoptws15;

import at.ac.tuwien.ac.heuoptws15.utils.PageEntry;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class KPMPSolutionWriter {
	public static void write(String path, KPMPSolution solution) throws IOException {
		try(Writer w = new BufferedWriter(new FileWriter(path))) {
			write(w, solution);
		}
	}
	
	private static void write(Writer w, KPMPSolution solution) throws IOException {
		List<Integer> spineOrder = solution.getSpineOrder();
		w.write(Integer.toString(spineOrder.size()));
		w.write('\n');
		w.write(Integer.toString(solution.getK()));
		w.write('\n');
		
		for(int i: spineOrder) {
			w.write(Integer.toString(i));
			w.write('\n');
		}
		
		for(Map.Entry<Integer, Set<Pair>> entry: solution.getEdgePartitionOnPage().entrySet()) {
			for(Pair e: entry.getValue()) {
				w.write(Integer.toString(e.getStart()));
				w.write(' ');
				w.write(Integer.toString(e.getEnd()));
				w.write(" [");
				w.write(Integer.toString(entry.getKey()));
				w.write("]\n");
			}
		}
	}
	
	public static void print(KPMPSolution solution) {
		try(Writer w = new BufferedWriter(new OutputStreamWriter(System.out))) {
			write(w, solution);
		} catch(IOException e) { e.printStackTrace(); }
	}
}

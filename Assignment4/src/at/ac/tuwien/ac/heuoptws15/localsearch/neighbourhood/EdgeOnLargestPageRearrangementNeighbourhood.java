package at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class EdgeOnLargestPageRearrangementNeighbourhood implements Neighbourhood {
    @Override
    public List<KPMPSolution> generate(KPMPSolution currentSolution) {
        List<KPMPSolution> neighbourhood = new LinkedList<>();
        Integer neighbourhoodSize = currentSolution.getK();
        for (int i = 0; i < neighbourhoodSize; i++) {
            KPMPSolution neighbour = currentSolution.clone();
            for (Pair edge : getRandomEdges(new ArrayList<>(neighbour.getEdgePartitionOnPage().get(i)),
                    neighbour.getEdgePartitionOnPage().get(i).size()/2)){
                neighbour.removeEdge(edge);
                Integer p = Utils.getPageWithMinCrossings(edge, neighbour);
                neighbour.addEdgeOnPage(edge.getStart(), edge.getEnd(), p);
            }
            neighbourhood.add(neighbour);
        }
        return neighbourhood;
    }

    private List<Pair> getRandomEdges(List<Pair> edges, Integer neighbourhoodSize) {
        Collections.shuffle(edges);
        return edges.stream().limit(neighbourhoodSize).collect(Collectors.toList());
    }
}

package at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.RandomizedConstructionHeuristic;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;
import com.sun.javafx.geom.Edge;

import java.util.*;

public class EdgeKSwapNeighbourhood implements Neighbourhood {

    private Random  rand;
    public EdgeKSwapNeighbourhood(){
        this.rand = new Random();
    }

    @Override
    public List<KPMPSolution> generate(KPMPSolution currentSolution) {

        KPMPSolution currentCopy = currentSolution.clone();
        currentCopy.setSortedEdges();

        //uzmi onaj sa pola distance i njih permutiraj
//        List<Integer> distances = currentCopy.getDistances();
//        int median = distances.get(distances.size()/2);
        int max = currentCopy.getDistances().size() - 1;
        int min = Math.min(3, max);
        int randomNum = rand.nextInt((max - min) + 1) + min;
        int dist = currentCopy.getDistances().get(randomNum);
        Set<Pair> selectedEdges = currentCopy.getDistancePair().get(dist);

        //oduzmes te edgeve i njihove crossinge i imas ih u zraku da s njima raspolazes
        //zatim s tim setom edgeva vrtis algoritam raspodjele na stranice s najmanje presjeka

        for (Pair edge : selectedEdges){
            currentCopy.removeEdge(edge);
        }

        List<KPMPSolution> neighbours = new LinkedList<>();

        int limit = Math.min(10, selectedEdges.size());

        while(neighbours.size() < limit) {
            List<Pair> shuffledEdges = new LinkedList<>();
            shuffledEdges.addAll(selectedEdges);
            Collections.shuffle(shuffledEdges);

            KPMPSolution neighbour = currentCopy.clone();

            for (Pair edge : shuffledEdges) {
                Integer page = Utils.getPageWithMinCrossings(edge, neighbour);
                neighbour.addEdgeOnPage(edge.getStart(), edge.getEnd(), page);
            }
            neighbours.add(neighbour);
        }
        return neighbours;
    }
}

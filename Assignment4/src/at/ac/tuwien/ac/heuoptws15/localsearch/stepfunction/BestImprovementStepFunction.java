package at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class BestImprovementStepFunction implements StepFunction {
    @Override
    public KPMPSolution getSolution(List<KPMPSolution> neighbourhood, KPMPSolution initialSolution) {
        KPMPSolution bestSolution = initialSolution;
        Iterator<KPMPSolution> iterator = neighbourhood.iterator();
        KPMPSolution current;
        while(iterator.hasNext()) {
            current = iterator.next();
//            System.out.println(current.getSpineOrder() +"      "+ current.getCrossings());
            if (bestSolution.compareTo(current) > 0) {
                bestSolution = current;
            }
        }
        return bestSolution;
    }
}

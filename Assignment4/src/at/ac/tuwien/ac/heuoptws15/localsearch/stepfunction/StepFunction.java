package at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;

import java.util.List;

public interface StepFunction {
    KPMPSolution getSolution(List<KPMPSolution> neighbourhood, KPMPSolution initialSolution);
}

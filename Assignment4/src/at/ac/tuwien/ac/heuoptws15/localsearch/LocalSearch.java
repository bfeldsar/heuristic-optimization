package at.ac.tuwien.ac.heuoptws15.localsearch;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.Neighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.StepFunction;

public abstract class LocalSearch {
    private Neighbourhood neighbourhood;
    private StepFunction stepFunction;

    public LocalSearch(Neighbourhood neighbourhood, StepFunction stepFunction){
        this.neighbourhood = neighbourhood;
        this.stepFunction = stepFunction;
    }

    public KPMPSolution localSearch(KPMPSolution initialSolution){
        Integer iterations = 0;
        KPMPSolution bestSolution = initialSolution;
        KPMPSolution previous;

        do {
            iterations++;
//            System.out.println(iterations);
            previous = bestSolution;
            bestSolution = stepFunction.getSolution(neighbourhood.generate(bestSolution), bestSolution);
            postIteration(bestSolution, previous);
        } while (stoppingCriteria(bestSolution, previous, iterations));
        System.out.println("Iterations: "+ iterations);
        return bestSolution;
    }

    abstract void postIteration(KPMPSolution bestSolution, KPMPSolution previous);

    abstract boolean stoppingCriteria(KPMPSolution bestSolution, KPMPSolution current, Integer iterations);
}

package at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.util.*;

public class VertexSwapNeighbourhood implements Neighbourhood {
    @Override
    public List<KPMPSolution> generate(KPMPSolution currentSolution) {
        List<KPMPSolution> neighbourhood = new LinkedList<>();
        Random random = new Random();
        Integer spineSize = currentSolution.getSpineOrder().size();
        Integer neighbourhoodSize = getNeighbourhoodSize(spineSize)*3;

        for (int i = 0; i < neighbourhoodSize; i++) {
            KPMPSolution neighbour = currentSolution.clone();

            int a = random.nextInt(spineSize);
            int b;
            do{
                b = random.nextInt(spineSize);
            }while (b == a);
//            int min = Integer.MAX_VALUE;
//            for(Integer e : currentSolution.getAdjacencyList().get(a)){
//                if (currentSolution.getAdjacencyList().get(e).size() < min){
//                    min = currentSolution.getAdjacencyList().get(e).size();
//                    b = e;
//                }
//            }
            swapVerteciesOnSpine(a, b, neighbour);
            neighbourhood.add(neighbour);
        }
        return neighbourhood;
    }

    private Integer getNeighbourhoodSize(Integer spineSize) {
        if (spineSize <= 5){
            return spineSize;
        }else if (spineSize <= 10){
            return spineSize-1;
        }else if (spineSize <= 50){
            return spineSize/2;
        }else if (spineSize <= 100){
            return spineSize/4;
        }else{
            return 50;
        }
    }

    private void swapVerteciesOnSpine(int a, int b, KPMPSolution neighbour) {

        // before swap
        int vertexA = neighbour.getSpineOrder().get(a);
        int vertexB = neighbour.getSpineOrder().get(b);
        Set<Pair> edges = findIncidentEdges(vertexA, vertexB, neighbour);
        removeIncidentEdges(edges, neighbour);

        // swap two vertices
        List<Integer> spine = neighbour.getSpineOrder();
        Integer temp = vertexA;
        spine.add(a, vertexB);
        spine.remove(a+1);
        spine.add(b, temp);
        spine.remove(b+1);

        // after swap
        for (Pair edge : edges) {
            Integer page = Utils.getPageWithMinCrossings(edge, neighbour);
            neighbour.addEdgeOnPage(edge.getStart(), edge.getEnd(), page);
        }
    }

    private void removeIncidentEdges(Set<Pair> edges, KPMPSolution neighbour) {
        for (Pair edge : edges){
            neighbour.removeEdge(edge);
        }
    }

    private Set<Pair> findIncidentEdges(int vertexA, int vertexB, KPMPSolution neighbour) {
        Set<Pair> edges = new HashSet<>();
        for (Integer adjacentVertex : neighbour.getAdjacencyList().get(vertexA)){
            edges.add(new Pair(adjacentVertex, vertexA));
        }
        for (Integer adjacentVertex : neighbour.getAdjacencyList().get(vertexB)){
            edges.add(new Pair(adjacentVertex, vertexB));
        }
        return edges;
    }

}

package at.ac.tuwien.ac.heuoptws15.variablesearch;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.EdgeKSwapNeighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.EdgeRearrangementNeighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.VertexSwapNeighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.BestImprovementStepFunction;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.RandomStepFunction;

public class GeneralVariableNeighbourhoodSearch extends VariableNeighbourhoodSearch {
    private VariableNeighbourhoodSearch vnd;

    public GeneralVariableNeighbourhoodSearch() {
        super(new RandomStepFunction());
        this.vnd = new VariableNeighbourhoodDescent(new BestImprovementStepFunction());
        this.vnd.addNeighbourhood(new EdgeKSwapNeighbourhood());
        this.vnd.addNeighbourhood(new VertexSwapNeighbourhood());
        this.vnd.addNeighbourhood(new EdgeRearrangementNeighbourhood());
    }

    @Override
    public KPMPSolution search(KPMPSolution initialSolution) {
        KPMPSolution bestSolution = initialSolution;
        int iterations = 0;
        int maxiterations = 10;
        do {
//            System.out.println(iterations);
            int k = 0;
            int kmax = this.getNeighbourhoods().size();
            while(k < kmax){
//                System.out.println(k+"   "+this.getNeighbourhoods().get(k).getClass() );
                KPMPSolution current = this.stepFunction.getSolution(this.getNeighbourhoods().get(k).generate(bestSolution), bestSolution);
                current = this.vnd.search(current);
                if (bestSolution.compareTo(current) > 0) {
                    bestSolution = current;
                    k = 0;
                } else {
                    k++;
                }
            }
       }while(iterations++ < maxiterations);
        return bestSolution;
    }
}

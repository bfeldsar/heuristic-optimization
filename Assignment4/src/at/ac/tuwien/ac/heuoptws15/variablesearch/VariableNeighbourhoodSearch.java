package at.ac.tuwien.ac.heuoptws15.variablesearch;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.Neighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.StepFunction;

import java.util.ArrayList;
import java.util.List;

public abstract class VariableNeighbourhoodSearch {
    private List<Neighbourhood> neighbourhoods;
    protected StepFunction stepFunction;

    public VariableNeighbourhoodSearch(StepFunction stepFunction) {
        this.stepFunction = stepFunction;
        this.neighbourhoods = new ArrayList<>();
    }

    public void addNeighbourhood(Neighbourhood neighbourhood){
        this.neighbourhoods.add(neighbourhood);
    }

    public void removeNeighbourhood(Neighbourhood neighbourhood){
        this.neighbourhoods.remove(neighbourhood);
    }

    public List<Neighbourhood> getNeighbourhoods() {
        return neighbourhoods;
    }

    public void setNeighbourhoods(List<Neighbourhood> neighbourhoods) {
        this.neighbourhoods = neighbourhoods;
    }

    public abstract KPMPSolution search(KPMPSolution initialSolution);
}

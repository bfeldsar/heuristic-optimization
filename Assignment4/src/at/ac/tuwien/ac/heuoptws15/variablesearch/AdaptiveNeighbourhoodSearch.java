package at.ac.tuwien.ac.heuoptws15.variablesearch;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.BestImprovementStepFunction;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class AdaptiveNeighbourhoodSearch extends VariableNeighbourhoodSearch {
    private interface DestroyMethod {
        KPMPSolution destroy(KPMPSolution solution);
    }
    private interface RepairMethod {
        KPMPSolution repair(KPMPSolution solution);
    }
    List<DestroyMethod> destroyMethods = new ArrayList<>();
    List<RepairMethod> repairMethods = new ArrayList<>();
    Set<Pair> edges = new HashSet<>();
    Set<Integer> vertices= new HashSet<>();

    public AdaptiveNeighbourhoodSearch() {
        super(new BestImprovementStepFunction());
    }

    @Override
    public KPMPSolution search(KPMPSolution initialSolution) {
        initDestroyMethods();
        initRepairMethods();
        List<Double> pDestroy = initWeights(this.destroyMethods.size());
        List<Double> pRepair = initWeights(this.repairMethods.size());
        KPMPSolution bestSolution = initialSolution;
        int iterations = 0;
        int maxiterations = 100;
        do{
            int destroyIndex = select(pDestroy);
            int repairIndex = select(pRepair);
            DestroyMethod destroyMethod = this.destroyMethods.get(destroyIndex);
            RepairMethod repairMethod = this.repairMethods.get(repairIndex);
            KPMPSolution current = repairMethod.repair(destroyMethod.destroy(bestSolution.clone()));
            if (bestSolution.compareTo(current) > 0) {
                bestSolution = current;
                iterations = 0;
            }else{
                iterations++;
            }
            edges.clear();
            vertices.clear();
            updateWeights(pDestroy, pRepair, destroyIndex, repairIndex, iterations == 0);
        }while(iterations < maxiterations);
        return bestSolution;
    }

    private void updateWeights(List<Double> pDestroy, List<Double> pRepair, int destroyIndex, int repairIndex, boolean better) {
        if (better){
            pDestroy.add(destroyIndex, pDestroy.get(destroyIndex) + 0.1);
            pDestroy.remove(destroyIndex+1);
            pRepair.add(repairIndex, pRepair.get(repairIndex) + 0.1);
            pRepair.remove(repairIndex+1);
        }
    }

    private void initRepairMethods() {
        this.repairMethods.add(solution -> {

            int max = solution.getSpineOrder().size();
            for(Integer vertex : vertices){
                int index = ThreadLocalRandom.current().nextInt(max);
                solution.addVertexOnIndex(index, vertex);
            }
            solution.updateSpineOrder();

            for (Pair edge : edges){
                int page = Utils.getPageWithMinCrossings(edge, solution);
                solution.addEdgeOnPage(edge.getStart(), edge.getEnd(),page);
            }
            return solution;
        });

        this.repairMethods.add(solution -> {

            int max = solution.getSpineOrder().size();
            for(Integer vertex : vertices){
                int index = ThreadLocalRandom.current().nextInt(max);
                solution.addVertexOnIndex(index, vertex);
            }
            solution.updateSpineOrder();

            for (Pair edge : edges){
                int page = Utils.getPageHeuristic(solution);
                solution.addEdgeOnPage(edge.getStart(), edge.getEnd(),page);
            }
            return solution;

        });
    }

    private void initDestroyMethods() {
        this.destroyMethods.add(solution -> {
            int k = 3;
            Set<Pair> alledges = new HashSet<>();
            while(k-- > 0){
                int n = ThreadLocalRandom.current().nextInt(solution.getSpineOrder().size());
                Integer vertex = solution.getSpineOrder().get(n);
                this.vertices.add(vertex);
                for (Integer v2 : solution.getAdjacencyList().get(vertex)) {
                    alledges.add(new Pair(vertex, v2));
                }
            }
            for (Pair edge : alledges){
                solution.removeEdge(edge);
                this.edges.add(edge);
            }
            for (Integer v : this.vertices){
                solution.getSpineIndexes().remove(v);
                solution.getSpineOrder().remove(v);
            }
            return solution;
        });
        this.destroyMethods.add(solution -> {
            int n = solution.getSpineOrder().size();
            Set<Integer> ver = new HashSet<>();
            ver.add(solution.getSpineOrder().get(0));
            ver.add(solution.getSpineOrder().get(n/2));
            ver.add(solution.getSpineOrder().get(n-1));
            Set<Pair> alledges = new HashSet<>();
            for(Integer vertex : ver) {
                this.vertices.add(vertex);
                for (Integer v2 : solution.getAdjacencyList().get(vertex)) {
                    alledges.add(new Pair(vertex, v2));
                }
            }
            for (Pair edge : alledges){
                solution.removeEdge(edge);
                this.edges.add(edge);
            }
            for (Integer v : this.vertices){
                solution.getSpineIndexes().remove(v);
                solution.getSpineOrder().remove(v);
            }
            return solution;
        });
        this.destroyMethods.add(solution -> {
            int k = solution.getEdgePage().size() / 3;
            List<Pair> lista = new ArrayList<>(solution.getEdgePage().keySet());
            Collections.shuffle(lista);
            for(Pair edge : lista.stream().limit(k).collect(Collectors.toList())){
                solution.removeEdge(edge);
                this.edges.add(edge);
            }
            return solution;
        });
        this.destroyMethods.add(solution -> {
            Set<Integer> distances = new HashSet<>();
            int len = solution.getSpineOrder().size();
            for (int i = len/2 - 2, n = len/2+2 ; i < n; i++){
                distances.add(i);
            }
            for (Pair edge : new ArrayList<>(solution.getEdgePage().keySet())){
                if (distances.contains(edge.getEnd()-edge.getStart())) {
                    solution.removeEdge(edge);
                    this.edges.add(edge);
                }
            }
            return solution;
        });
    }

    private List<Double> initWeights(Integer n) {
        List<Double> l = new ArrayList<>();

        while(n-->0){
            l.add(1.0);
        }
        return l;
    }

    private int select(List<Double> weights){
        double sum = weights.stream().mapToDouble(a -> a).sum();
        double randDbl = ThreadLocalRandom.current().nextDouble(sum);
        double accsum = 0;
        int i = 0;
        for(Double weight: weights){
            accsum+= weight;
            if (accsum >=randDbl){
                break;
            }
            i++;
        }
        return i;
    }
}
;
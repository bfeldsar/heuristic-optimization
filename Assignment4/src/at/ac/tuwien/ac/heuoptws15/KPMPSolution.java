package at.ac.tuwien.ac.heuoptws15;

import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.util.*;

public class KPMPSolution implements Comparable<KPMPSolution> {
    private int K = 0;
    private Long crossings;
    private List<Integer> spineOrder = new ArrayList<>();
    private Map<Integer, Set<Pair>> edgePartitionOnPage = new HashMap<>();
    private HashMap<Integer, Long> pageCrossings = new HashMap<>();
    private boolean[][] adjacencyMatrix;
    private List<List<Integer>> adjacencyList;
    private List<Pair> sortedEdges;
    private Map<Integer, Set<Pair>> distancePair;
    private List<Integer> distances;
    private HashMap<Pair, Integer> edgePage = new HashMap<>();
    private Map<Integer, Integer> spineIndexes = new HashMap<>();

    public KPMPSolution(int K, boolean[][] adjacencyMatrix) {
        this.K = K;
        this.adjacencyMatrix = adjacencyMatrix;
        for (int i = 0 ; i < K; i++){
            edgePartitionOnPage.put(i, new HashSet<>());
            pageCrossings.put(i, 0L);
        }
        this.crossings = 0L;
    }

    public KPMPSolution(int K, boolean[][] adjacencyMatrix, List<List<Integer>> adjacencyList) {
        this(K, adjacencyMatrix);
        this.adjacencyList = adjacencyList;
    }


    public void setSpineOrder(List<Integer> spineOrder) {
        this.spineOrder = spineOrder;
        updateSpineOrder();
    }

    public void addVertexOnIndex(Integer index, Integer vertex){
        this.spineOrder.add(index,vertex);
    }

    public void updateSpineOrder(){
        for (int i = 0, n = spineOrder.size(); i < n; i++){
            spineIndexes.put(spineOrder.get(i), i);
        }
    }

    public void addEdgeOnPage(int vertexA, int vertexB, int page) {
        Pair newPair = new Pair(vertexA, vertexB);
        incrementCrossings(newPair, page);
        edgePartitionOnPage.get(page).add(newPair);
        edgePage.put(newPair, page);
    }

    public void removeEdge(Pair edge){
        int page = edgePage.get(edge);
        edgePartitionOnPage.get(page).remove(edge);
        edgePage.remove(edge);
        decrementCrossings(edge,page);
    }

    public long getMaxEgesOnPage() {
        int max = 0;
        for (Integer page : edgePartitionOnPage.keySet()) {
            int tmp = edgePartitionOnPage.get(page).size();
            if (max < tmp) {
                max = tmp;
            }
        }
        return max;
    }

    public List<Integer> getSpineOrder() {
        return spineOrder;
    }


    public Map<Integer, Set<Pair>> getEdgePartitionOnPage() {
        return edgePartitionOnPage;
    }

    public int getK() {
        return K;
    }

    public HashMap<Pair, Integer> getEdgePage() {
        return edgePage;
    }

    public Long getCrossings() {
        return crossings;
    }

    public void setCrossings(Long crossings) {
        this.crossings = crossings;
    }

    public void setEdgePartitionOnPage(Map<Integer, Set<Pair>> edgePartitionOnPage) {
        this.edgePartitionOnPage = edgePartitionOnPage;
    }

    public void setDistancePair(Map<Integer, Set<Pair>> distancePair) {
        this.distancePair = distancePair;
    }

    public void setDistances(List<Integer> distances) {
        this.distances = distances;
    }

    public void setEdgePage(HashMap<Pair, Integer> edgePage) {
        this.edgePage = edgePage;
    }

    public void setSortedEdges(){
        if (this.sortedEdges == null) {
            this.sortedEdges = getSortedEdgesByDistance();
        }
    }

    private List<Pair> getSortedEdgesByDistance() {
        Map<Integer, Integer> vertexIndex = new HashMap<>();
        for (int i = 0; i < spineOrder.size(); i++) {
            vertexIndex.put(spineOrder.get(i), i);
        }
        //po adjency matrix i dodavat parove (min, max) u mapu gdje je udaljenost key
        distancePair = new HashMap<>();

        for (int i = 0, n = spineOrder.size() - 1; i < n; i++) {
            for (int j = i + 1; j < spineOrder.size(); j++) {
                if(adjacencyMatrix[i][j]) {
                    int dist = Math.abs(vertexIndex.get(j) - vertexIndex.get(i));
                    Set<Pair> pairs = distancePair.getOrDefault(dist, new HashSet<>());
                    pairs.add(new Pair(i, j));
                    distancePair.put(dist, pairs);
                }
            }
        }
        distances = new ArrayList<>();
        distances.addAll(distancePair.keySet());
        distances.sort((a, b) -> a.compareTo(b));

        List<Pair> sortedPairs = new LinkedList<>();

        for (Integer dist : distances){
                sortedPairs.addAll(distancePair.get(dist));
        }

        return sortedPairs;
    }

    public Map<Integer, Set<Pair>> getDistancePair() {
        return distancePair;
    }

    public List<Integer> getDistances() {
        return distances;
    }

    private void incrementCrossings(Pair newPair, Integer page){
//        Set<Integer> between = Utils.getVerticesBetween(newPair, spineIndexes, spineOrder);
//        long n1 = Utils.getCrossingsOnPage(edgePartitionOnPage.get(page), newPair, between);
        long n = this.edgePartitionOnPage.get(page).stream().
                filter(pair -> Utils.existsCrossing(pair, newPair, spineOrder)).count();
        this.pageCrossings.put(page, this.pageCrossings.get(page) + n);
        this.crossings += n;
    }


    private void decrementCrossings(Pair oldPair, Integer page){
//        Set<Integer> between = Utils.getVerticesBetween(oldPair, spineIndexes, spineOrder);
//        long n = Utils.getCrossingsOnPage(edgePartitionOnPage.get(page), oldPair, between);
        long n = this.edgePartitionOnPage.get(page).stream().
                filter(pair -> Utils.existsCrossing(pair, oldPair, spineOrder)).count();
        this.pageCrossings.put(page, this.pageCrossings.get(page) - n);
        this.crossings -= n;
    }

    public KPMPSolution clone(){
        KPMPSolution cloned = new KPMPSolution(this.K, this.adjacencyMatrix);
        cloned.setCrossings(this.crossings);
        cloned.setSpineOrder(new LinkedList<>(this.spineOrder));
        cloned.setEdgePartitionOnPage(cloneMap(this.edgePartitionOnPage));
        cloned.setEdgePage((HashMap<Pair, Integer>) this.edgePage.clone());
        cloned.setPageCrossings((HashMap<Integer, Long>) this.pageCrossings.clone());
        if (this.distancePair != null){
            cloned.setDistancePair(cloneMap(this.distancePair));
        }
        if (this.distances != null) {
            cloned.setDistances(new LinkedList<>(this.distances));
        }
        if (this.sortedEdges != null){
            cloned.sortedEdges = new LinkedList<>(this.sortedEdges);
        }
        cloned.setAdjacencyList(this.getAdjacencyList());
        return cloned;
    }

    @Override
    public int compareTo(KPMPSolution o) {
        return crossings.compareTo(o.getCrossings());
    }

    public void decreaseCrossings(Long n) {
        this.crossings -= n;
    }


    public void increaseCrossings(Long n) {
        this.crossings += n;
    }

    public Map<Integer, Long> getPageCrossings() {
        return pageCrossings;
    }

    public void setPageCrossings(HashMap<Integer, Long> pageCrossings) {
        this.pageCrossings = pageCrossings;
    }

    public List<List<Integer>> getAdjacencyList() {
        return adjacencyList;
    }

    public void setAdjacencyList(List<List<Integer>> adjacencyList) {
        this.adjacencyList = adjacencyList;
    }

    private Map<Integer, Set<Pair>> cloneMap(Map<Integer, Set<Pair>> map){
        Map<Integer, Set<Pair>> mapnew = new HashMap<>();
        for (Map.Entry<Integer, Set<Pair>> entry : map.entrySet()){
            mapnew.put(entry.getKey(), new HashSet<>(entry.getValue()));
        }
        return mapnew;
    }

    @Override
    public String toString() {
        return edgePartitionOnPage.toString()+"\n"+spineOrder+"\n"+crossings;
    }

    public boolean[][] getAdjacencyMatrix() {
        return adjacencyMatrix;
    }

    public Map<Integer, Integer> getSpineIndexes() {
        return spineIndexes;
    }

    public void setSpineIndexes(Map<Integer, Integer> spineIndexes) {
        this.spineIndexes = spineIndexes;
    }
}

package at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class EdgeRearrangementNeighbourhood implements Neighbourhood {
    @Override
    public List<KPMPSolution> generate(KPMPSolution currentSolution) {
        List<KPMPSolution> neighbourhood = new LinkedList<>();
        List<Pair> allEdges = new ArrayList<>();
        for (Set<Pair> value : currentSolution.getEdgePartitionOnPage().values()){
            allEdges.addAll(value);
        }
        Integer neighbourhoodSize = allEdges.size();
        for (int i = 0; i < neighbourhoodSize; i++) {
            KPMPSolution neighbour = currentSolution.clone();
            for (Pair edge : getRandomEdges(allEdges, neighbourhoodSize)){
                neighbour.removeEdge(edge);
                Integer page = Utils.getPageWithMinCrossings(edge, neighbour);
                neighbour.addEdgeOnPage(edge.getStart(), edge.getEnd(), page);
            }
            neighbourhood.add(neighbour);
        }
        return neighbourhood;
    }

    private List<Pair> getRandomEdges(List<Pair> edges, Integer neighbourhoodSize) {
        Collections.shuffle(edges);
        return edges.stream().limit(neighbourhoodSize).collect(Collectors.toList());
    }
}

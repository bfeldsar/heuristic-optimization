package at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class FirstImprovementStepFunction implements StepFunction{

    @Override
    public KPMPSolution getSolution(List<KPMPSolution> neighbourhood, KPMPSolution initialSolution) {
        Iterator<KPMPSolution> iterator = neighbourhood.iterator();
        KPMPSolution current;
        while(iterator.hasNext()) {
            current = iterator.next();
            if (initialSolution.compareTo(current) > 0){
                return current;
            }
        }
        return initialSolution;
    }
}

package at.ac.tuwien.ac.heuoptws15.localsearch;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolution;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.DeterministicConstructionHeuristic;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.RandomizedConstructionHeuristic;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.Neighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.StepFunction;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.io.IOException;
import java.util.*;

public class IteratedLocalSearch extends LocalSearch {
    private Integer localMinimumLimit;
    private Integer localMinimumCounter;
    private Integer maxIteration;

    public IteratedLocalSearch(Neighbourhood neighbourhood, StepFunction stepFunction, Integer localMinimumLimit, Integer maxIteration) {
        super(neighbourhood, stepFunction);
        this.localMinimumLimit = localMinimumLimit;
        this.maxIteration = maxIteration;
        this.localMinimumCounter = 0;

    }

    @Override
    void postIteration(KPMPSolution bestSolution, KPMPSolution previous) {
        if (bestSolution == previous){
            localMinimumCounter++;
            bestSolution = jumpFromLocalMinimum(previous);
//            System.out.println("iterated "+ localMinimumCounter);
        }
    }

    private KPMPSolution jumpFromLocalMinimum(KPMPSolution currentSolution) {
        KPMPInstance instance = new KPMPInstance(currentSolution);
        KPMPSolution solution = new KPMPSolution(instance.getK(), instance.getAdjacencyMatrix());
        List<Integer> spineOrder = Utils.getRandomSpineOrder(instance);
        Set<Integer> visited = new HashSet<>();
        solution.setSpineOrder(spineOrder);
        solution.setAdjacencyList(instance.getAdjacencyList());
        Map<Integer, Integer> spineMap = new HashMap<>();
        for (int i = 0; i < spineOrder.size(); i++){
            spineMap.put(spineOrder.get(i), i);
        }
        Map<Integer, Set<Pair>> groupedEdges = Utils.groupEdges(spineMap, instance.getAdjacencyList());
        for (int i = spineOrder.size(); i >= 0 ; i--){
            for(Pair pair : groupedEdges.get(i)){
                Integer page = Utils.getPageWithMinCrossings(pair, solution); // random.nextInt(instance.getK());
                if (page == null) continue;
                solution.addEdgeOnPage(pair.getStart(), pair.getEnd(), page);
            }
        }
        return solution;
    }

    @Override
    boolean stoppingCriteria(KPMPSolution bestSolution, KPMPSolution current, Integer iterations) {
        return maxIteration > iterations && localMinimumLimit > localMinimumCounter;
    }
}

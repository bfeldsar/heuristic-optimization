package at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction;

import at.ac.tuwien.ac.heuoptws15.KPMPSolution;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class RandomStepFunction implements StepFunction {
    @Override
    public KPMPSolution getSolution(List<KPMPSolution> neighbourhood, KPMPSolution initialSolution) {
        Random random = new Random();
        int num = random.nextInt(neighbourhood.size());
        int i = 0;
        Iterator<KPMPSolution> iterator = neighbourhood.iterator();
        KPMPSolution current = iterator.next();
        while(i < num){
            if(i++ == num){
                return current;
            }
            current = iterator.next();
        }
        return initialSolution;
    }
}

package at.ac.tuwien.ac.heuoptws15;

import at.ac.tuwien.ac.heuoptws15.constructionheuristic.ConstuctionHeuristic;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.DeterministicConstructionHeuristic;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.DeterministicGroupedEdgesConstructionHeuristic;
import at.ac.tuwien.ac.heuoptws15.constructionheuristic.DeterministicGroupedEdgesUpdatedConstructionHeuristic;
import at.ac.tuwien.ac.heuoptws15.localsearch.IteratedLocalSearch;
import at.ac.tuwien.ac.heuoptws15.localsearch.LocalSearch;
import at.ac.tuwien.ac.heuoptws15.localsearch.SimpleLocalSearch;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.EdgeKSwapNeighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.EdgeRearrangementNeighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.RandKEdgesPage;
import at.ac.tuwien.ac.heuoptws15.localsearch.neighbourhood.VertexSwapNeighbourhood;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.BestImprovementStepFunction;
import at.ac.tuwien.ac.heuoptws15.localsearch.stepfunction.FirstImprovementStepFunction;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        ConstuctionHeuristic strategy = new DeterministicGroupedEdgesUpdatedConstructionHeuristic();
        for (int j = 1; j < 11; j++) {
            if (j==6) continue;
            String fileName = "automatic-"+j+".txt";
            KPMPSolution bestSolution = null;
            Long totalTime = 0L;
            final Integer N = 5;
            System.out.println(fileName);
            for (int k = 0; k < N; k++){
                KPMPInstance instance = KPMPInstance.readInstance("./instances/"+fileName);
                Long t1 = System.currentTimeMillis();
                KPMPSolution solution = strategy.solve(instance);
                System.out.println((System.currentTimeMillis() - t1));
                LocalSearch localSearch = new IteratedLocalSearch(new VertexSwapNeighbourhood(),
                        new BestImprovementStepFunction(), 30, 50);
                solution = localSearch.localSearch(solution);
                if (bestSolution == null || bestSolution.compareTo(solution) > 0){
                    bestSolution = solution;
                }
                Long t2 = (System.currentTimeMillis() - t1);
                System.out.println((k+1)+". time  -  "+ t2+",  crossings - " +solution.getCrossings());
                totalTime += t2;
            }
            System.out.println(fileName);
            System.out.println("Avarage time:  " + totalTime/N);
            System.out.println("best solution #crossings after local search: "+ bestSolution.getCrossings());
            System.out.println("------------------------------------------------------------------------------------------");
            KPMPSolutionWriter.write("./results/deterministic/"+fileName, bestSolution);
        }
    }
}

//System.out.println("Average time: "+ timeSum/N);
//System.out.println("Best result: " + Collections.min(crossingsPerRun));
//System.out.println("Mean: " + Utils.findMean(crossingsPerRun).getAsDouble());
//System.out.println("Standard deviation: " + Utils.findDeviation(crossingsPerRun));
//System.out.println("------------------------------------------------------------------------");
//bestSolution.write(Paths.get("./results/randomized/"+fileName).toString());

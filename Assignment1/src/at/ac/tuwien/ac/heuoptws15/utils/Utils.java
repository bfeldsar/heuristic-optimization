package at.ac.tuwien.ac.heuoptws15.utils;

import java.util.*;
import java.util.stream.Collectors;

public class Utils {

    public static Double findDeviation(Collection<Long> collection) {
        Double mean = findMean(collection).getAsDouble();
        Double squareSum = 0.0;
        for (Long num : collection) {
            squareSum += Math.pow(num - mean, 2);
        }
        return Math.sqrt((squareSum) / (collection.size() - 1));
    }

    public static OptionalDouble findMean(Collection<Long> collection) {
        return collection.stream().mapToDouble(a -> a).average();
    }

    public static Long getCrossingsNumber(Map<Integer, Long> pageCrossings) {
        return pageCrossings.values().stream().mapToLong(Long::longValue).sum();
    }

    public static Set<Integer> getEmptyPages(Map<Integer, Set<Pair>> pageEdges) {
        return pageEdges.keySet().stream().filter(
                page -> pageEdges.get(page) == null || (pageEdges.get(page) != null && pageEdges.isEmpty())).collect(Collectors.toSet());
    }

    public static boolean existsCrossing(Pair edge1, Pair edge2, List<Integer> spineOrder) {
        Integer x11 = spineOrder.indexOf(edge1.start);
        Integer x12 = spineOrder.indexOf(edge1.end);
        Integer x21 = spineOrder.indexOf(edge2.start);
        Integer x22 = spineOrder.indexOf(edge2.end);
        return crossing(x11, x12, x21, x22) || crossing(x21, x22, x11, x12);
    }

    private static boolean crossing(Integer x11, Integer x12, Integer x21, Integer x22) {
        Integer min1 = Math.min(x11, x12);
        Integer max1 = Math.max(x11, x12);
        Integer min2 = Math.min(x21, x22);
        return min1 < min2 && min2 < max1 && Math.max(x21, x22) > max1;
    }
}

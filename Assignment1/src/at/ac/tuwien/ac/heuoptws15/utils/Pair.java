package at.ac.tuwien.ac.heuoptws15.utils;

public class Pair {
    int start;
    int end;

    public Pair(int start, int end){
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair pair = (Pair) o;

        return equals(pair) || equals(new Pair(pair.end, pair.start));
    }

    public boolean contains(int vertex){
        return start == vertex || end == vertex;
    }

    private boolean equals(Pair pair) {
        return start == pair.start && end == pair.end;
    }

    @Override
    public int hashCode() {
        int result = start;
        result = 31 * result + end;
        return result;
    }

    @Override
    public String toString() {
        return "("+this.start+","+this.end+")";
    }
}

package at.ac.tuwien.ac.heuoptws15.stategy;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolutionWriter;

import java.io.IOException;

public interface ConstuctionHeuristic {
    void solve(KPMPInstance instance, String fileName) throws IOException;
}

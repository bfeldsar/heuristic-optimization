package at.ac.tuwien.ac.heuoptws15.stategy;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolutionWriter;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.io.IOException;
import java.nio.channels.InterruptedByTimeoutException;
import java.nio.file.Paths;
import java.util.*;

public class RandomizedConstructionHeuristic implements ConstuctionHeuristic {

    private static List<Integer> getSpineOrder(KPMPInstance instance, Random random){
        List<Integer> spine = new ArrayList<>();
        Set<Integer> spineSet = new HashSet<>();
        List<Integer> notVisited = new ArrayList<>();
        Integer n = instance.getNumVertices();
        for (int i = 0; i < n; i++){
            notVisited.add(i);
        }
        Integer current = n / 2;
        while(spine.size() != n){
            spine.add(current);
            notVisited.remove(current);
            spineSet.add(current);
            List<Integer> neighbours = new ArrayList<>(instance.getAdjacencyList().get(current));
            if((current = getNotVisitedNeighbour(neighbours, notVisited, random)) == null){
                current = notVisited.size() != 0 ? notVisited.get(random.nextInt(notVisited.size())) : null;
            }
        }
        return spine;
    }

    private static Integer getNotVisitedNeighbour(List<Integer> neighbours, List<Integer> notVisited, Random random) {
        neighbours.retainAll(notVisited);
        if (neighbours.isEmpty()){
            return null;
        }else{
            return neighbours.get(random.nextInt(neighbours.size()));
        }
    }


    @Override
    public void solve(KPMPInstance instance, String fileName) throws IOException {
        Long timeSum = 0L;
        List<Long> crossingsPerRun = new ArrayList<>();
        final Integer N = 10;
        KPMPSolutionWriter bestWriter = null;
        Long best = Long.MAX_VALUE;
        for (int j = 0 ; j < N; j++){
            KPMPSolutionWriter writer = new KPMPSolutionWriter(instance.getK());
            Long t1 = System.currentTimeMillis();
            Random random = new Random();
            List<Integer> spineOrder = getSpineOrder(instance, random);
            Set<Integer> visited = new HashSet<>();
            Map<Integer, Set<Pair>> pageEdges = new HashMap<>();
            Map<Integer, Long> pageCrossings = new HashMap<>();
            for (int i = 0; i < instance.getK(); i++){
                pageEdges.put(i, new HashSet<>());
                pageCrossings.put(i, 0L);
            }
            for (Integer v1: spineOrder){
                visited.add(v1);
                for(Integer v2 : instance.getAdjacencyList().get(v1)){
                    if (!visited.contains(v2)){
                        Integer page = getPageWithMinCrossings(new Pair(v2, v1), spineOrder, pageEdges,
                                pageCrossings, random); // random.nextInt(instance.getK());
                        if (page == null) continue;
                        writer.addEdgeOnPage(v1, v2, page);
                        Set<Pair> edges = pageEdges.get(page);
                        edges.add(new Pair(v2, v1));
                        pageEdges.put(page, edges);
                    }
                }
            }
            writer.setSpineOrder(spineOrder);
            Long time = System.currentTimeMillis() - t1;
            timeSum += time;
            Long curr = Utils.getCrossingsNumber(pageCrossings);
            crossingsPerRun.add(curr);
            if (best > curr){
                best = curr;
                bestWriter = writer;
            }
        }
        System.out.println(fileName);
        System.out.println("Average time: "+ timeSum/N);
        System.out.println("Best result: " + Collections.min(crossingsPerRun));
        System.out.println("Mean: " + Utils.findMean(crossingsPerRun).getAsDouble());
        System.out.println("Standard deviation: " + Utils.findDeviation(crossingsPerRun));
        System.out.println("------------------------------------------------------------------------");
        bestWriter.write(Paths.get("./results/randomized/"+fileName).toString());
    }

    public static Integer getPageWithMinCrossings(Pair pair, List<Integer> spineOrder, Map<Integer, Set<Pair>> pageEdges,
                                                  Map<Integer, Long> pageCrossings, Random random) {
        Long min = Long.MAX_VALUE;
        Set<Integer> emptyPages = Utils.getEmptyPages(pageEdges);
        Set<Integer> minimumPages = new HashSet<>();
        for (Map.Entry<Integer, Set<Pair>> entry : pageEdges.entrySet()) {
            Long crossings = 0L;
            for (Pair edge : entry.getValue()){
                if(Utils.existsCrossing(edge, pair, spineOrder)){
                    crossings++;
                }
            }
            if (crossings == 0){
                return entry.getKey();
            }
            if (crossings + pageCrossings.get(entry.getKey()) < min) {
                min = crossings + pageCrossings.get(entry.getKey());
                minimumPages.clear();
                minimumPages.add(entry.getKey());
            }else if (crossings + pageCrossings.get(entry.getKey()) == min) {
                minimumPages.add(entry.getKey());
            }
        }
        Integer page = null;
        if (min == 0 && !minimumPages.isEmpty()){
            page = new ArrayList<>(minimumPages).get(random.nextInt(minimumPages.size()));
            min = 1L;
        }else if (!emptyPages.isEmpty()){
            page = new ArrayList<>(emptyPages).get(random.nextInt(minimumPages.size()));
            min = 1L;
        }else{
            page = new ArrayList<>(minimumPages).get(random.nextInt(minimumPages.size()));
        }
        pageCrossings.put(page, min);
        return page;
    }
}

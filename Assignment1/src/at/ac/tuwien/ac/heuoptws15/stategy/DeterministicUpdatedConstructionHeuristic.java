package at.ac.tuwien.ac.heuoptws15.stategy;

import at.ac.tuwien.ac.heuoptws15.KPMPInstance;
import at.ac.tuwien.ac.heuoptws15.KPMPSolutionWriter;
import at.ac.tuwien.ac.heuoptws15.utils.Pair;
import at.ac.tuwien.ac.heuoptws15.utils.Utils;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class DeterministicUpdatedConstructionHeuristic implements ConstuctionHeuristic {

    @Override
    public void solve(KPMPInstance instance, String fileName) throws IOException {
        KPMPSolutionWriter writer = new KPMPSolutionWriter(instance.getK());
        Long t1 = System.currentTimeMillis();
        List<Integer> spineOrder = getSpineOrder(instance);
        Map<Integer, Integer> spineIndexes = new HashMap<>();
        for (int i = 0, n = spineOrder.size(); i < n; i++){
            spineIndexes.put(spineOrder.get(i), i);
        }
        Set<Integer> visited = new HashSet<>();
        Map<Integer, Set<Pair>> pageEdges = new HashMap<>();
        Map<Integer, Long> pageCrossings = new HashMap<>();
        for (int i = 0; i < instance.getK(); i++){
            pageEdges.put(i, new HashSet<>());
            pageCrossings.put(i, 0L);
        }
        for (Integer v1: spineOrder){
            visited.add(v1);
            for(Integer v2 : instance.getAdjacencyList().get(v1)){
                if (!visited.contains(v2)){
                    Integer page = getPageWithMinCrossings(new Pair(v2, v1), spineOrder, pageEdges, pageCrossings, spineIndexes); // random.nextInt(instance.getK());
                    if (page == null) continue;
                    writer.addEdgeOnPage(v1, v2, page);
                    Set<Pair> edges = pageEdges.get(page);
                    edges.add(new Pair(v2, v1));
                    pageEdges.put(page, edges);
                }
            }
        }
        writer.setSpineOrder(spineOrder);
        System.out.println(fileName);
        System.out.println(System.currentTimeMillis() - t1);
//        System.out.println(spineOrder);
//        System.out.println(pageEdges);
        System.out.println("#crossings = " + Utils.getCrossingsNumber(pageCrossings));
        System.out.println("------------------------------------------------------------------------------");
        writer.write(Paths.get("./results/deterministic/"+fileName).toString());
    }

    public static Integer getPageWithMinCrossings(Pair pair, List<Integer> spineOrder, Map<Integer, Set<Pair>> pageEdges,
                                                  Map<Integer, Long> pageCrossings, Map<Integer, Integer> spineIndexes) {
        Integer page = null;
        Long min = Long.MAX_VALUE;
        Set<Integer> emptyPages = Utils.getEmptyPages(pageEdges);
        Set<Integer> between = new HashSet<>();
        int start = spineIndexes.get(pair.getStart());
        int end = spineIndexes.get(pair.getEnd());
        int m = Math.max(start, end);
        for (int i=Math.min(start, end)+1; i < m; i++){
           between.add(spineOrder.get(i));
        }
        for (Map.Entry<Integer, Set<Pair>> entry : pageEdges.entrySet()) {
            Long crossings = getCrossingsOnPage(entry.getValue(), pair, between);
            if (crossings == 0){
                return entry.getKey();
            }
            if (crossings < min) {
                min = crossings;
                page = entry.getKey();
            }
        }
        if (!emptyPages.isEmpty()){
            page = emptyPages.iterator().next();
            min = 1L;
        }
        pageCrossings.put(page, pageCrossings.get(page) + min);
        return page;
    }

    private static Long getCrossingsOnPage(Set<Pair> edges, Pair pair, Set<Integer> between) {
        return edges.stream().filter(e -> crossing(between, pair, e)).count();
    }

    private static boolean crossing(Set<Integer> between, Pair pair, Pair e) {
        return !(pair.contains(e.getStart()) || pair.contains(e.getEnd())) &&
                ((between.contains(e.getStart()) && !between.contains(e.getEnd()))
                || (between.contains(e.getEnd()) && !between.contains(e.getStart())));
    }

    private static List<Integer> getSpineOrder(KPMPInstance instance){
        List<Integer> spine = new ArrayList<>();
        Set<Integer> spineSet = new HashSet<>();
        Integer n = instance.getNumVertices();
        Integer current = n / 2;
        while(spine.size() != n){
            spine.add(current);
            spineSet.add(current);
            List<Integer> neighbours = instance.getAdjacencyList().get(current);
            if((current = getNotVisitedNeighbour(neighbours, spineSet)) == null){
                current = getNotVisitedVertex(instance, spineSet);
            }
        }
        return spine;
    }

    private static Integer getNotVisitedVertex(KPMPInstance instance, Set<Integer> spineSet) {
        for (int i = 0; i<instance.getNumVertices(); i++){
            if (!spineSet.contains(i)){
                return i;
            }
        }
        return null;
    }

    private static Integer getNotVisitedNeighbour(List<Integer> neighbours, Set<Integer> spine) {
        for (Integer neighbour : neighbours){
            if (!spine.contains(neighbour)){
                return neighbour;
            }
        }
        return null;
    }

//


    //    private static Long getCrossingsNumber(List<Integer> spineOrder, Map<Integer, Set<Pair>> pageEdges) {
//        Long counter = 0L;
//        for (Map.Entry<Integer, Set<Pair>> entry : pageEdges.entrySet()){
//            counter += getCrossingsOnPage(entry.getValue(), spineOrder);
//        }
//        return counter;
//    }
//
//    private static Long getCrossingsOnPage(Set<Pair> edges, List<Integer> spineOrder) {
//        Long counter = 0L;
//        Set<Pair> visited = new HashSet<>();
//        for(Pair edge1 : edges){
//            visited.add(edge1);
//            for(Pair edge2 : edges){
//                if (!visited.contains(edge2) && existsCrossing(edge1, edge2, spineOrder)) {
//                    counter++;
//                }
//            }
//        }
//        return counter;
//        }
}

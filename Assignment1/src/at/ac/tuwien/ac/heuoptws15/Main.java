package at.ac.tuwien.ac.heuoptws15;

import at.ac.tuwien.ac.heuoptws15.stategy.*;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        ConstuctionHeuristic strategy = new DeterministicGroupedEdgesUpdatedConstructionHeuristic();
        for (int j = 1; j < 11; j++) {
            String fileName = "automatic-"+j+".txt";
            KPMPInstance instance = KPMPInstance.readInstance("./instances/"+fileName);
            strategy.solve(instance, fileName);
        }
    }
}
